package modelo;

public class DetallesPedido {

	private int idPedido;
	private int idArticulo;
	private int cantidad;
	
	private Pedido pedido; //es de 1 pedido
	private Articulo articulo;  //es de 1 articulo
	
	public DetallesPedido() {
		super();
	}
	public DetallesPedido(int idPedido, int idArticulo, int cantidad, Pedido pedido, Articulo articulo) {
		super();
		this.idPedido = idPedido;
		this.idArticulo = idArticulo;
		this.cantidad = cantidad;
		this.pedido = pedido;
		this.articulo = articulo;
	}

	public int getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}
	public int getIdArticulo() {
		return idArticulo;
	}
	public void setIdArticulo(int idArticulo) {
		this.idArticulo = idArticulo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Pedido getPedido() {
		return pedido;
	}
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}


	public Articulo getArticulo() {
		return articulo;
	}


	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}

}
