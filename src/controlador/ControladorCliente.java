package controlador;

import modelo.Cliente;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import modelo.ModeloCliente;
import vista.Principal;
import vista.cliente.ConsultarCliente;
import vista.cliente.BorrarCliente;
//import vista.cliente.FormularioDeModificado;
import vista.cliente.ListarClientes;
import vista.cliente.ListarClientesPedidos;
import vista.cliente.NuevoCliente;
import vista.cliente.GestionCliente;

public class ControladorCliente {

	// un atributo por cada ventana de Cliente
	// un atributo para el modelo Cliente

	private ModeloCliente modeloCliente;

	private GestionCliente gestionCliente;
	private Principal principal;
	private NuevoCliente nuevoCliente;
	private ConsultarCliente consultarCliente;
	private BorrarCliente borrarCliente;
	//private FormularioDeModificado formDeModificado;
	private ListarClientes listarClientes;
	private ListarClientesPedidos listarClientesPedidos;

	

	public ListarClientesPedidos getListarClientesPedidos() {
		return listarClientesPedidos;
	}

	public void setListarClientesPedidos(ListarClientesPedidos listarClientesPedidos) {
		this.listarClientesPedidos = listarClientesPedidos;
	}

	public ConsultarCliente getConsultarCliente() {
		return consultarCliente;
	}

	public void setConsultarCliente(ConsultarCliente consultarCliente) {
		this.consultarCliente = consultarCliente;
	}

	public NuevoCliente getNuevoCliente() {
		return nuevoCliente;
	}

	public void setNuevoCliente(NuevoCliente nuevoCliente) {
		this.nuevoCliente = nuevoCliente;
	}

	public BorrarCliente getBorrarCliente() {
		return borrarCliente;
	}

	public void setBorrarCliente(BorrarCliente borrarCliente) {
		this.borrarCliente = borrarCliente;
	}

	public ListarClientes getListarClientes() {
		return listarClientes;
	}

	public void setListarClientes(ListarClientes listarClientes) {
		this.listarClientes = listarClientes;
	}

	public void insertarCliente(String id,String nombre, String direccion, String codPostal, String telefono, String string) {

		Cliente cliente = new Cliente();

		cliente.setId(id);
		cliente.setNombre(nombre);
		cliente.setDireccion(direccion);
		cliente.setCodPostal(codPostal);
		cliente.setTelefono(telefono);

		try {
			this.modeloCliente.insertar(cliente);

			JOptionPane.showMessageDialog(null, "CLIENTE INSERTADO EN LA BD");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR AL INSERTAR CLIENTE");
		}
	}

	public void abrirGestionCliente() {
		this.gestionCliente.setVisible(true);
	}


	// borrado de cliente
	public void abrirFormDeBorrado() {
		ArrayList<Cliente> clientes = this.modeloCliente.seleccionarTodos();

		borrarCliente.rellenarComboClientes(clientes);
		borrarCliente.setVisible(true);
	}

	public void rellenarFormDeBorrado(String idCliente) {
		Cliente cliente = this.modeloCliente.select(idCliente);
		borrarCliente.rellenarFormulario(cliente);
	}

	public void eliminarCliente(String string) {
		this.modeloCliente.borrar(string);
		borrarCliente.mostrarMensaje("Cliente destruido con exito");
		borrarCliente.clearForm();

		// actualizar formulario
		ArrayList<Cliente> clientes = this.modeloCliente.seleccionarTodos();
		borrarCliente.rellenarComboClientes(clientes);
	}


	public ModeloCliente getModeloCliente() {
		return modeloCliente;
	}

	public void setModeloCliente(ModeloCliente modeloCliente) {
		this.modeloCliente = modeloCliente;
	}

	public GestionCliente getGestionCliente() {
		return gestionCliente;
	}

	public void setGestionCliente(GestionCliente gestionCliente) {
		this.gestionCliente = gestionCliente;
	}

	public Principal getPrincipal() {
		return principal;
	}

	public void setPrincipal(Principal principal) {
		this.principal = principal;
	}

	
	public void modificarCliente(String id, String nombre,String direccion, String codPostal,
			String telefono) {
		Cliente cliente = new Cliente();

		cliente.setNombre(nombre);
		cliente.setDireccion(direccion);
		cliente.setCodPostal(codPostal);
		cliente.setTelefono(telefono);

		this.modeloCliente.modificar(cliente);
	}


	public void abrirFormListar() {
		
		ArrayList<Cliente> clientes=new ArrayList<Cliente>(); 
		
		clientes=modeloCliente.seleccionarTodos();
		
		//listarClientes.rellenarTabla(clientes);
		
		listarClientes.setVisible(true);	
	}

	public void abrirListarClientesPedidos() {
	
		
	}

	

}
