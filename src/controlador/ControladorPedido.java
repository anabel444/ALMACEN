package controlador;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import modelo.Cliente;
import modelo.DetallesPedido;
import modelo.ModeloCliente;
import modelo.ModeloPedido;
import modelo.Pedido;
import vista.pedido.*;

public class ControladorPedido {
	
	private ModeloPedido modeloPedido;
	private ModeloCliente modeloCliente;
	
	private BorrarPedido borrarPedido;
	private ConsultarPedido consultarPedido;
	private GestionPedido gestionPedido;
	private ListarPedidos listarPedidos;
	private NuevoPedido nuevoPedido;
		
	public ModeloPedido getModeloPedido() {
		return modeloPedido;
	}
	public void setModeloPedido(ModeloPedido modeloPedido) {
		this.modeloPedido = modeloPedido;
	}
	public BorrarPedido getBorrarPedido() {
		return borrarPedido;
	}
	public void setBorrarPedido(BorrarPedido borrarPedido) {
		this.borrarPedido = borrarPedido;
	}
	public ConsultarPedido getConsultarPedido() {
		return consultarPedido;
	}
	public void setConsultarPedido(ConsultarPedido consultarPedido) {
		this.consultarPedido = consultarPedido;
	}
	public GestionPedido getGestionPedido() {
		return gestionPedido;
	}
	public void setGestionPedido(GestionPedido gestionPedido) {
		this.gestionPedido = gestionPedido;
	}
	public ListarPedidos getListarPedidos() {
		return listarPedidos;
	}
	public void setListarPedidos(ListarPedidos listarPedidos) {
		this.listarPedidos = listarPedidos;
	}
	public NuevoPedido getNuevoPedido() {
		return nuevoPedido;
	}
	public void setNuevoPedido(NuevoPedido nuevoPedido) {
		this.nuevoPedido = nuevoPedido;
	}
	public void abrirListarPedidos() {
		ArrayList<Pedido> pedidos;

		try {
			//listarPedidos.limpiarTabla();
			pedidos = modeloPedido.seleccionarTodos();
			listarPedidos.rellenarTabla(pedidos);

			listarPedidos.setVisible(true);
		
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR AL SELECCIONAR PEDIDOS");
		}

	}
	public void abrirNuevoPedido() {
		// TODO Auto-generated method stub
		
	}
	public void abrirBorrarPedido() {
		// TODO Auto-generated method stub
		
	}
	public void abrirConsultarPedido() {
		ArrayList<Pedido> pedidos=new ArrayList<Pedido>();
		try {
			pedidos=modeloPedido.seleccionarTodos();
			consultarPedido.rellenarTabla(pedidos);
			consultarPedido.setVisible(true);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR AL SELECCIONAR PEDIDOS");
		}
		
	}
	public void abriGestionPedido() {
		gestionPedido.setVisible(true);
		
	}
	public void rellenarTablaDetallesPedido(int idPedido) {
		ArrayList<DetallesPedido> detallesPedidos;

		try {
			
			detallesPedidos = modeloPedido.seleccionarDetallesPedido(idPedido);
			listarPedidos.rellenarTablaDetallesPedido(detallesPedidos);
		}catch (Exception e){
			JOptionPane.showMessageDialog(null, "ERROR AL SELECCIONAR DETALLES PEDIDOS");
		}
		
	}
	
	public void rellenarDatosCliente(String id) {
		Cliente cliente=new Cliente();
		try {
			cliente=modeloCliente.seleccionarDatosCliente(id);
			listarPedidos.rellenarCliente(cliente);
		}catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR AL SELECCIONAR CLIENTE");
		}
		
		
		
		
	}
	public ModeloCliente getModeloCliente() {
		return modeloCliente;
	}
	public void setModeloCliente(ModeloCliente modeloCliente) {
		this.modeloCliente = modeloCliente;
	}
	
	
	

	
	
}
