package vista.pedido;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import controlador.ControladorPedido;
import modelo.Cliente;
import modelo.DetallesPedido;
import modelo.Pedido;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ListarPedidos extends JDialog {

	private final JPanel contentPanel = new JPanel();

	private ControladorPedido controladorPedido;
	private JTable tabla;
	
	private JTextField nombre;
	private JTextField direccion;
	private JTextField codPostal;
	private JTextField telefono;
	private JTable tablaDetalle;


	public ControladorPedido getControladorPedido() {
		return controladorPedido;
	}

	public void setControladorPedido(ControladorPedido controladorPedido) {
		this.controladorPedido = controladorPedido;
	}

	/**
	 * Create the dialog.
	 */
	public ListarPedidos(JDialog parent, boolean modal) {

		super(parent, modal);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		addMouseListener(new MouseAdapter() {
			
			
		});
		setBounds(100, 100, 1055, 547);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblListadoPedidos = new JLabel("LISTADO PEDIDOS");
		lblListadoPedidos.setHorizontalAlignment(SwingConstants.CENTER);
		lblListadoPedidos.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblListadoPedidos.setBounds(95, 11, 227, 29);
		contentPanel.add(lblListadoPedidos);
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(23, 51, 401, 199);
			contentPanel.add(scrollPane);
			{
				tabla = new JTable();
				tabla.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						
						int row = tabla.getSelectedRow();

						if (row >= 0 ) {
							int idPedido=Integer.parseInt(String.valueOf(tabla.getModel().getValueAt(row, 0)));
							String idCliente=String.valueOf(tabla.getModel().getValueAt(row, 1));
				
							controladorPedido.rellenarTablaDetallesPedido(idPedido);
							controladorPedido.rellenarDatosCliente(idCliente);
							
						}
					}
				});
				scrollPane.setViewportView(tabla);
			}
		}

		JLabel lblDetallesDelPedidos = new JLabel("DETALLES DEL PEDIDO SELECCIONADO");
		lblDetallesDelPedidos.setHorizontalAlignment(SwingConstants.CENTER);
		lblDetallesDelPedidos.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDetallesDelPedidos.setBounds(504, 11, 308, 29);
		contentPanel.add(lblDetallesDelPedidos);

		JLabel lblClienteDelPedido = new JLabel("CLIENTE DEL PEDIDO SELECCIONADO");
		lblClienteDelPedido.setHorizontalAlignment(SwingConstants.CENTER);
		lblClienteDelPedido.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblClienteDelPedido.setBounds(276, 338, 296, 29);
		contentPanel.add(lblClienteDelPedido);

		nombre = new JTextField();
		nombre.setColumns(10);
		nombre.setBounds(137, 396, 163, 20);
		contentPanel.add(nombre);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(76, 399, 82, 14);
		contentPanel.add(lblNombre);

		direccion = new JTextField();
		direccion.setColumns(10);
		direccion.setBounds(384, 396, 188, 20);
		contentPanel.add(direccion);

		JLabel lblDireccion = new JLabel("Direccion");
		lblDireccion.setBounds(324, 399, 82, 14);
		contentPanel.add(lblDireccion);

		codPostal = new JTextField();
		codPostal.setColumns(10);
		codPostal.setBounds(643, 396, 86, 20);
		contentPanel.add(codPostal);

		JLabel lblCodPostal = new JLabel("Cod Postal");
		lblCodPostal.setBounds(582, 399, 82, 14);
		contentPanel.add(lblCodPostal);

		telefono = new JTextField();
		telefono.setColumns(10);
		telefono.setBounds(800, 396, 86, 20);
		contentPanel.add(telefono);

		JLabel lblTelfono = new JLabel("Tel\u00E9fono");
		lblTelfono.setBounds(739, 399, 82, 14);
		contentPanel.add(lblTelfono);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(480, 51, 467, 238);
		contentPanel.add(scrollPane);
		
		tablaDetalle = new JTable();
		scrollPane.setViewportView(tablaDetalle);
	}

	public void rellenarTabla(ArrayList<Pedido> pedidos) {

		DefaultTableModel dtm =new DefaultTableModel();
		
		String[] encabezados = { "ID", "IDCLIENTE", "FECHA" };

		dtm.setColumnIdentifiers(encabezados);
		
		for (Pedido pedido : pedidos) {
			
			String[] fila = { String.valueOf(pedido.getId()), pedido.getIdCliente(),
					String.valueOf(pedido.getFecha()) };

			dtm.addRow(fila);
		}
		tabla.setModel(dtm);
		TableRowSorter<DefaultTableModel> modeloOrdenado = new TableRowSorter<DefaultTableModel>(dtm);
		tabla.setRowSorter(modeloOrdenado);
	}

	public void rellenarTablaDetallesPedido(ArrayList<DetallesPedido> detallesPedido) {
		
		DefaultTableModel dtm=new DefaultTableModel();
		
		tablaDetalle.removeAll();
		
		ArrayList<DetallesPedido> detallesPedidos;
		String[] encabezados = { "ID", "IDART", "ARTICULO", "CANTIDAD" };

		dtm.setColumnIdentifiers(encabezados);
		
		for (DetallesPedido detalle : detallesPedido) {
			String[] fila = { String.valueOf(detalle.getIdPedido()), String.valueOf(detalle.getIdArticulo()),
					detalle.getArticulo().getNombre(), String.valueOf(detalle.getCantidad()) };

			dtm.addRow(fila);
		}
		tablaDetalle.setModel(dtm);
		TableRowSorter<DefaultTableModel> modeloOrdenado = new TableRowSorter<DefaultTableModel>(dtm);
		tablaDetalle.setRowSorter(modeloOrdenado);

	}


	public void rellenarCliente(Cliente cliente) {
		nombre.setText(cliente.getNombre());
		direccion.setText(cliente.getDireccion());
		codPostal.setText(cliente.getCodPostal());
		telefono.setText(cliente.getTelefono());
		
	}

	public void limpiarTabla() {
		
		tabla.setModel(new DefaultTableModel());
		tablaDetalle.setModel(new DefaultTableModel());

		nombre.setText("");
		codPostal.setText("");
		direccion.setText("");
		telefono.setText("");
		
	}
}
