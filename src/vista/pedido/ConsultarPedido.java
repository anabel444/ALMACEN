package vista.pedido;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import controlador.ControladorPedido;
import modelo.Pedido;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class ConsultarPedido extends JDialog {

	private final JPanel contentPanel = new JPanel();

	private ControladorPedido controladorPedido;
	private JTable tabla;
	private JScrollPane scrollPane;
	
	public ControladorPedido getControladorPedido() {
		return controladorPedido;
	}

	public void setControladorPedido(ControladorPedido controladorPedido) {
		this.controladorPedido = controladorPedido;
	}

	/**
	 * Create the dialog.
	 */
	public ConsultarPedido(JDialog parent, boolean modal) {
		
		super(parent,modal);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 434, 261);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		{
			scrollPane = new JScrollPane();
			scrollPane.setBounds(42, 11, 354, 214);
			contentPanel.add(scrollPane);
			{
				tabla = new JTable();
				scrollPane.setViewportView(tabla);
			}
		}
	}

	public void rellenarTabla(ArrayList<Pedido> pedidos) {
		DefaultTableModel dtm =new DefaultTableModel();
		
		String[] encabezados = { "ID", "IDCLIENTE", "FECHA" };
		dtm.setColumnIdentifiers(encabezados);
		
		for (Pedido pedido : pedidos) {
			String[] fila = { String.valueOf(pedido.getId()), pedido.getIdCliente(),
					String.valueOf(pedido.getFecha()) };

			dtm.addRow(fila);
		}
		tabla.setModel(dtm);
		TableRowSorter<DefaultTableModel> modeloOrdenado = new TableRowSorter<DefaultTableModel>(dtm);
		tabla.setRowSorter(modeloOrdenado);
		
	}

}
